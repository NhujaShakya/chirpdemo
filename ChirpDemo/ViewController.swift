//
//  ViewController.swift
//  ChirpDemo
//
//  Created by Nhuja Shakya on 11/27/19.
//  Copyright © 2019 Nhuja Shakya. All rights reserved.
//

import UIKit
import ChirpSDK

class ViewController: UIViewController {
    
    @IBOutlet weak var senderLbl: UITextField!
    @IBOutlet weak var receiberLbl: UILabel!
    
    
    
    private let chirp: ChirpSDK = ChirpSDK(appKey: CHIRP_APP_KEY, andSecret: CHIRP_APP_SECRET)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        if let err = chirp.start(in: ChirpAudioMode.send) {
          print("ChirpError (%@)", err.localizedDescription)
        } else {
          print("Started ChirpSDK")
        }
        
        initalSetup()
    }
    
    func initalSetup(){
       if let err = chirp.setConfig(CHIRP_APP_CONFIG) {
         print("ChirpError (%@)", err.localizedDescription)
       } else {
         if let err = chirp.start() {
           print("ChirpError (%@)", err.localizedDescription)
         } else {
           print("Started ChirpSDK")
         }
       }
        
        chirp.receivedBlock = {
          (data : Data?, channel: UInt?) -> () in
          if data != nil {
            let identifier = String(data: data!, encoding: .ascii)
            self.receiberLbl.text = identifier
            return
          } else {
            print("ChirpError: Decode failed!")
            return
          }
        }
    }
    
    @IBAction func sendClicked(_ sender: Any) {
        
        
        guard let text = senderLbl.text, !text.isEmpty else {
            simpleAlert(title: "Error", message: "Write your message!")
            return
            
        }
        let data: Data = text.data(using: .utf8)!
            if let err = chirp.send(data) {
              print("ChirpError (%@)", err.localizedDescription)
            } else {
              print("Sent (identifier)")
            }
        
    }
        
}

