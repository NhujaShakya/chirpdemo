//
//  _ ViewController.swift
//  ChirpDemo
//
//  Created by Nhuja Shakya on 11/27/19.
//  Copyright © 2019 Nhuja Shakya. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func simpleAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert,animated: true, completion: nil)
        
    }
}

extension UIImage {

    func isEqualsTo(_ image: UIImage?) -> Bool {
        if let _image = image {
         return self.pngData() == _image.pngData()
        }else{
            return false
        }
    }

}

